import classes from "./SelectWidget.module.css";
import {Select} from "../../../Select/Select";

export function SelectWidget({title, list}) {
    const items = [
        {title: 'forest', value: {}},
        {title: 'ocean', value: {}},
        {title: 'party', value: {}},
        {title: 'rainbow', value: {}},
        {title: 'sky', value: {}},
        {title: 'party2', value: {}},
        {title: 'rainbow', value: {}},
        {title: 'sky', value: {}},
        {title: 'party2', value: {}},
    ]

    return (
        <div className="widget">
            <div className={`${classes.widget__header} widget__header`}>
                <div className="widget__title">
                    {title}
                </div>
            </div>
            <div className='widgetContent'>
                <Select items={items} selectedIndex={3}/>
            </div>
        </div>
    );
}