import classes from "./TextInputWidget.module.css";

export function TextInputWidget({title, value}) {
    return (
        <div className="widget">
            <div className='widget__header'>
                <div className="widget__title">
                    {title}
                </div>
            </div>
            <div className='widgetContent'>
                <input type='text'/>
            </div>
        </div>
    );
}