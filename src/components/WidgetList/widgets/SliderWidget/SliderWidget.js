import classes from "./SliderWidget.module.css";
import {Slider} from "../../../common/Slider/Slider";
import {useState} from "react";

export function SliderWidget({title, value, minValue, maxValue, step}) {
    const [_value, setValue] = useState(value)

    return (
        <div className="widget">
            <div className={`${classes.widget__header} widget__header`}>
                <div className="widget__title">
                    {title}
                </div>
                <div className={classes.widget__value}>
                    {_value}
                </div>
            </div>
            <div className='widgetContent'>
                <Slider value={value} min={minValue} max={maxValue} step={step} onValueChange={value => {
                    setValue(value)
                }}/>
            </div>
        </div>
    );
}