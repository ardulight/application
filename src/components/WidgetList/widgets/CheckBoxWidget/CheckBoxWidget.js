import classes from "./CheckBoxWidget.module.css";
import {Switch} from "../../../common/Switch/Switch";

export function CheckBoxWidget({title}) {
    return (
        <div className="widget">
            <div className={`${classes.widget__header} widget__header`}>
                <div className="widget__title">
                    {title}
                </div>
                <Switch></Switch>
            </div>
        </div>
    );
}