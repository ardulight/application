import classes from "./LabelWidget.module.css";

export function LabelWidget({title, value}) {
    return (
        <div className="widget">
            <div className='widget__header'>
                <div className="widget__title">
                    {title}
                </div>
            </div>
            <div className='widgetContent'>
                <div className={classes.Label}>
                    {value}
                </div>
            </div>
        </div>
    );
}