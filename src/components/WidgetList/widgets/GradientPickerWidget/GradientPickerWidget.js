import classes from "../SliderWidget/SliderWidget.module.css";
import {Slider} from "../../../common/Slider/Slider";
import {GradientPicker} from "../../../common/GradientPicker/GradientPicker";

export function GradientPickerWidget({title}) {
    return (
        <div className="widget">
            <div className={`${classes.widget__header} widget__header`}>
                <div className="widget__title">
                    {title}
                </div>
            </div>
            <div className='widgetContent'>
                <GradientPicker/>
            </div>
        </div>
    );
}