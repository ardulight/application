import classes from "./WidgetList.module.css"
import {SliderWidget} from "./widgets/SliderWidget/SliderWidget";
import {SelectWidget} from "./widgets/SelectWidget/SelectWidget";
import {LabelWidget} from "./widgets/LabelWidget/LabelWidget";
import {TextInputWidget} from "./widgets/TextInputWidget/TextInputWidget";
import {CheckBoxWidget} from "./widgets/CheckBoxWidget/CheckBoxWidget";
import {ColorPickerWidget} from "./widgets/ColorPickerWidget/ColorPickerWidget";
import {GradientPickerWidget} from "./widgets/GradientPickerWidget/GradientPickerWidget";

export function WidgetList({title, widgets}) {
    return (
        <div className={classes.widgetList}>
            <div className={classes.title}>
                <h1>Solid color</h1>
            </div>
            <div className={classes.widget}>
                <GradientPickerWidget title={'Palette picker'}/>
            </div>
            <div className={classes.widget}>
                <ColorPickerWidget title={'Color picker'}/>
            </div>
            <div className={classes.widget}>
                <SliderWidget title={'Slider'} minValue={0} maxValue={100} step={1} value={75}/>
            </div>
            <div className={classes.widget}>
                <SelectWidget title={'Select'}/>
            </div>
            <div className={classes.widget}>
                <LabelWidget title={'Label'} value={'100%'}/>
            </div>
            <div className={classes.widget}>
                <TextInputWidget title={'Text input'}/>
            </div>
            <div className={classes.widget}>
                <CheckBoxWidget title={'Check box'}/>
            </div>
        </div>
    );
}