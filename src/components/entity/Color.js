export class Color {
    _rgb = {r: 0, g: 0, b: 0}
    _hex = 'ffffff'
    _hsl = {h: 0, s: 0, l: 0}
    _hsv = {h: 0, s: 0, v: 0}

    constructor() {
    }

    get rgb() {
        return this._rgb
    }

    set rgb({r, g, b}) {
        this._rgb = {r, g, b}
        this._hex = this._rgbToHex({r, g, b})
        this._hsl = this._rgbToHsl({r, g, b})
        this._hsv = this._rgbToHsv({r, g, b})
    }

    get hex() {
        return this._hex
    }

    set hex(hex) {
        this._hex = hex
        this._rgb = this._hexToRgb(hex)
        this._hsl = this._rgbToHsl(this._rgb)
        this._hsv = this._rgbToHsv(this._rgb)
    }

    get hsl() {
        return this._hsl
    }

    set hsl({h, s, l}) {
        this._hsl = {h, s, l}
        this._rgb = this._hslToRgb({h, s, l})
        this._hex = this._rgbToHex(this._rgb)
        this._hsv = this._rgbToHsv(this._rgb)
    }

    set hsv({h, s, v}) {
        this._hsv = {h, s, v}
        this._rgb = this._hsvToRgb({h, s, v})
        this._hex = this._rgbToHex(this._rgb)
        this._hsl = this._rgbToHsl(this._rgb)
    }

    get hsv() {
        return this._hsv
    }

    _rgbToHex({r, g, b}) {
        return ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
    }

    _hexToRgb(hex) {
        let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
        if (result) {
            return {r: parseInt(result[1], 16), g: parseInt(result[2], 16), b: parseInt(result[3], 16)}
        }

        return 'ffffff'
    }

    _rgbToHsl({r, g, b}) {
        r /= 255;
        g /= 255;
        b /= 255;

        let max = Math.max(r, g, b), min = Math.min(r, g, b);
        let h, s, l = (max + min) / 2;

        if (max === min) {
            h = s = 0;
        } else {
            let d = max - min;
            s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

            switch (max) {
                case r:
                    h = (g - b) / d + (g < b ? 6 : 0);
                    break;
                case g:
                    h = (b - r) / d + 2;
                    break;
                case b:
                    h = (r - g) / d + 4;
                    break;
            }

            h /= 6;
        }

        return {h: h * 360, s: s * 100, l: l * 100};
    }

    _hslToRgb(h, s, l) {
        let r, g, b;

        if (s === 0) {
            r = g = b = l; // achromatic
        } else {
            function hue2rgb(p, q, t) {
                if (t < 0) t += 1;
                if (t > 1) t -= 1;
                if (t < 1 / 6) return p + (q - p) * 6 * t;
                if (t < 1 / 2) return q;
                if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
                return p;
            }

            var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            var p = 2 * l - q;

            r = hue2rgb(p, q, h + 1 / 3);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1 / 3);
        }

        return {r: r * 255, g: g * 255, b: b * 255};
    }

    _rgbToHsv({r, g, b}) {
        r /= 255; g /= 255; b /= 255;

        var max = Math.max(r, g, b), min = Math.min(r, g, b);
        var h, s, v = max;

        var d = max - min;
        s = max === 0 ? 0 : d / max;

        if (max === min) {
            h = 0; // achromatic
        } else {
            switch (max) {
                case r:
                    h = (g - b) / d + (g < b ? 6 : 0);
                    break;
                case g:
                    h = (b - r) / d + 2;
                    break;
                case b:
                    h = (r - g) / d + 4;
                    break;
            }

            h /= 6;
        }

        return {h: h * 360, s: s * 100, v: v * 100};
    }

    _hsvToRgb({h, s, v}) {
        h /= 360
        s /= 100
        v /= 100

        let r, g, b;

        let i = Math.floor(h * 6);
        let f = h * 6 - i;
        let p = v * (1 - s);
        let q = v * (1 - f * s);
        let t = v * (1 - (1 - f) * s);

        switch (i % 6) {
            case 0: r = v; g = t; b = p; break;
            case 1: r = q; g = v; b = p; break;
            case 2: r = p; g = v; b = t; break;
            case 3: r = p; g = q; b = v; break;
            case 4: r = t; g = p; b = v; break;
            case 5: r = v; g = p; b = q; break;
        }

        //return {r: r * 255, g: g * 255, b: b * 255};

        return {r: Math.floor(r * 255), g: Math.floor(g * 255), b: Math.floor(b * 255)};
    }
}