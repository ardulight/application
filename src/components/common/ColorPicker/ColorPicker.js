import classes from "./ColorPicker.module.css";
import {HueSlider} from "./HueSlider/HueSlider";
import {useState} from "react";
import {SaturationValueSlider} from "./SaturationValueSlider/SaturationValueSlider";
import {Color} from "../../entity/Color";

export function ColorPicker({checked}) {
    const HEX_PATTERN = /^(([a-fA-F]|[0-9]){2}){3}$/

    const [color, setColor] = useState(new Color())

    const [hex, setHEX] = useState(color.hex)

    const setRgbColor = ({r, g, b}) => {
        r = r ?? 0
        g = g ?? 0
        b = b ?? 0

        let color = new Color()
        color.rgb = {r, g, b}

        setHEX(color.hex)
        setColor(color)
    }

    const setHexColor = (event) => {
        setHEX(event.target.value)

        if (!HEX_PATTERN.test(event.target.value))
            return

        let color = new Color()
        color.hex = event.target.value

        setColor(color)
    }

    const setHue = (hue) => {
        let newColor = new Color()
        newColor.hsv = {...color.hsv, h: hue}

        setHEX(newColor.hex)
        setColor(newColor)
    }

    const setHsv = (color) => {
        setHEX(color.hex)
        setColor(color)
    }

    return (
        <div className={classes.ColorPicker}>
            <div className={classes.HSVArea}>
                <div>
                    <SaturationValueSlider
                        color={color}
                        onValueChange={setHsv}/>
                </div>
                <div>
                    <HueSlider
                        hue={color.hsv.h}
                        onValueChange={setHue}/>
                </div>
            </div>
            <div className={classes.TextsFields}>
                <div>
                    <div className={classes.FieldTitle}>HEX</div>
                </div>
                <div>
                    <input type='text'
                           pattern="(([a-fA-F]|[0-9]){2}){3}"
                           value={hex}
                           onChange={setHexColor}/>
                </div>
                <div>
                    <div className={classes.FieldTitle}>Red</div>
                </div>
                <div>
                    <input type='number'
                           min={0}
                           max={255}
                           value={color.rgb.r}
                           onChange={event => {
                               setRgbColor({...color.rgb, r: parseInt(event.target.value)})
                           }}
                           />
                </div>
                <div>
                    <div className={classes.FieldTitle}>Green</div>
                </div>
                <div>
                    <input type='number'
                           min={0}
                           max={255}
                           value={color.rgb.g}
                           onChange={event => {
                               setRgbColor({...color.rgb, g: parseInt(event.target.value)})
                           }}
                           />
                </div>
                <div>
                    <div className={classes.FieldTitle}>Blue</div>
                </div>
                <div>
                    <input type='number'
                           min={0}
                           max={255}
                           value={color.rgb.b}
                           onChange={event => {
                               setRgbColor({...color.rgb, b: parseInt(event.target.value)})
                           }}
                           />
                </div>
            </div>
        </div>
    );
}