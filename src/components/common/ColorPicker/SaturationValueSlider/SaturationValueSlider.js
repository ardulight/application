
import classes from "./SaturationValueSlider.module.css";
import {useRef, useState} from "react";
import {hsvToHsl} from "../../../../utils/ColorUtil";
import {Color} from "../../../entity/Color";

export function SaturationValueSlider({onValueChange, onValueChanged, color}) {
    const min = 0
    const max = 100

    const ref = useRef();

    let lastValue = color

    const startMove = () => {
        window.addEventListener('mousemove', updateValue)
        window.addEventListener('mouseup', endMove)
        window.addEventListener('selectstart', disableSelect)
    }

    const endMove = () => {
        window.removeEventListener('mouseup', endMove)
        window.removeEventListener('mousemove', updateValue)
        window.removeEventListener('selectstart', disableSelect);
        if (onValueChanged)
            onValueChanged(lastValue)
    }

    const disableSelect = (event) => {
        event.preventDefault()
    }

    const updateValue = (e) => {
        let valueY = 1 - (e.clientY - ref.current.getBoundingClientRect().top) / ref.current.offsetHeight
        let valueX = (e.clientX - ref.current.getBoundingClientRect().left) / ref.current.offsetWidth

        if (valueY > 1) {
            valueY = 1
        } else if (valueY < 0) {
            valueY = 0
        }

        if (valueX > 1) {
            valueX = 1
        } else if (valueX < 0) {
            valueX = 0
        }

        valueY = valueY * (max - min) + min
        valueX = valueX * (max - min) + min

        if (valueY !== lastValue.hsv.v || valueX !== lastValue.hsv.s) {
            let color = new Color()
            color.hsv = {...lastValue.hsv, v: valueY, s: valueX}
            if (onValueChange)
                onValueChange(color)
        }
    }

    let percentY = (color.hsv.v - min) / (max - min) * 100
    let percentX = (color.hsv.s - min) / (max - min) * 100

    let sliderColor = hsvToHsl({h: color.hsv.h, s: color.hsv.s, v: color.hsv.v})
    let colorString = `hsl(${sliderColor.h} ${sliderColor.s}% ${sliderColor.l}%)`

    return (

        <div ref={ref} className={classes.SliderArea} onMouseDown={e => startMove()}>
            <div className={classes.SliderContainer} style={{bottom: `${percentY}%`, left: `${percentX}%`}}>
                <div className={classes.Slider} style={{backgroundColor: colorString}}/>
            </div>
            <div ref={ref} className={classes.CommonBackground} style={{backgroundColor: `hsl(${color.hsv.h}deg 100% 50%)`}}>
                <div className={classes.Background1}/>
                <div className={classes.Background2}/>
            </div>
        </div>
    );
}