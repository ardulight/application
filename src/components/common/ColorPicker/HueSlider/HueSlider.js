import {useRef, useState} from "react";
import classes from "./HueSlider.module.css"


export function HueSlider({onValueChange, onValueChanged, hue}) {
    const min = 0
    const max = 360

    const sliderRef = useRef();
    let lastValue = hue

    const startMove = () => {
        window.addEventListener('mousemove', updateValue)
        window.addEventListener('mouseup', endMove)
        window.addEventListener('selectstart', disableSelect)
    }

    const endMove = () => {
        window.removeEventListener('mouseup', endMove)
        window.removeEventListener('mousemove', updateValue)
        window.removeEventListener('selectstart', disableSelect);
        if (onValueChanged)
            onValueChanged(lastValue)
    }

    const disableSelect = (event) => {
        event.preventDefault()
    }

    const updateValue = (e) => {
        let value = (e.clientY - sliderRef.current.getBoundingClientRect().top) / sliderRef.current.offsetHeight

        if (value > 1)
            value = 1
        else if (value < 0)
            value = 0

        value = Math.round((value * (max - min) + min))

        if (value !== lastValue) {
            lastValue = value
            if (onValueChange)
                onValueChange(value)
        }
    }

    let percent = (hue - min) / (max - min) * 100

    //console.log(percent)

    return (

        <div ref={sliderRef} className={classes.SliderArea} onMouseDown={e => startMove()}>
            <div className={classes.SliderContainer} style={{top: `${percent}%`}}>
                <div className={classes.Slider} style={{backgroundColor: `hsl(${hue}deg 100% 50%)`}}/>
            </div>
        </div>
    );
}