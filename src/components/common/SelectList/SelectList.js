import classes from "./SelectList.module.css";

export function SelectList({items, selectedIndex, onSelect}) {
    return (
        <div className={classes.SelectList}>
            {
                items.map((value, index) => (
                    <div className={classes.Item} key={index}>
                        {
                            selectedIndex == index && <div className={classes.Marker}/>
                        }
                        <button
                            onClick={() => onSelect(index, value)}
                            className={`${classes.ItemButton} ${selectedIndex == index ? classes.Selected : null}`}>
                            {value.label}
                        </button>
                    </div>
                ))
            }
        </div>
    );
}