import classes from "./Switch.module.css";

export function Switch({checked}) {
    return (
        <label className={classes.ToggleSwitch}>
            <input className={`${classes.ToggleSwitchInput}`} type="checkbox"/>
            <span className={`${classes.Slider}`}/>
        </label>
    );
}