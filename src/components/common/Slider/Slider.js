import classes from "./Slider.module.css";
import {useRef, useState} from "react";

export function Slider({onValueChange, onValueChanged, value, min = 0, max = 100, step = 1}) {
    if (!value){
        value = max
    }

    const ref = useRef();
    const [_value, setValue] = useState(value)
    let prevValue = value

    const startMove = () => {
        window.addEventListener('mousemove', updateValue)
        window.addEventListener('mouseup', endMove)
        window.addEventListener('selectstart', disableSelect)
    }

    const endMove = () => {
        window.removeEventListener('mouseup', endMove)
        window.removeEventListener('mousemove', updateValue)
        window.removeEventListener('selectstart', disableSelect);
        if (onValueChanged)
            onValueChanged(value)
    }

    const disableSelect = (event) => {
        event.preventDefault()
    }

    const updateValue = (e) => {
        let value = (e.clientX - ref.current.offsetLeft) / ref.current.offsetWidth

        if (value > 1){
            value = 1
        }
        else if (value < 0){
            value = 0
        }

        value = Math.round((value * (max - min) + min) / step) * step

        if (value !== prevValue){
            setValue(value)
            if (onValueChange)
                onValueChange(value)
            prevValue = value
        }
    }

    let percent = (_value - min) / (max - min) * 100

    return (
        <div ref={ref} className={classes.SliderArea} onMouseDown={e => startMove()}>
            <div className={classes.Fill} style={{width: `${percent}%`}}>
                <div className={classes.Slider}>
                </div>
            </div>
        </div>
    );
}