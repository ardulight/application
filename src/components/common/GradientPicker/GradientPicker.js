import classes from "./GradientPicker.module.css";
import {useRef, useState} from "react";

export function GradientPicker({onValueChange, onValueChanged}) {
    const ref = useRef();
    const [gradient, setGradient] = useState([
        {percent: 0, color: 'ff0fff'},
        {percent: 50, color: 'ff0f00'},
        {percent: 100, color: '0000ff'},
    ])

    const updateColor = (index, color) => {
        console.log(index, color)
        let newGradient = [...gradient]
        newGradient[index].color = color
        setGradient(newGradient)
    }

    const startMove = (index) => {
        const _updateValue = (event) => {
            updateValue(index, event)
        }

        const disableSelect = (event) => {
            event.preventDefault()
        }

        const endMove = () => {
            console.log("up")
            window.removeEventListener('mouseup', endMove)
            window.removeEventListener('mousemove', _updateValue)
            window.removeEventListener('selectstart', disableSelect);
            //if (onValueChanged)
            //    onValueChanged(value)
        }

        window.addEventListener('mousemove', _updateValue)
        window.addEventListener('mouseup', endMove)
        window.addEventListener('selectstart', disableSelect)
    }

    const updateValue = (index, e) => {
        let value = (e.clientX - ref.current.getBoundingClientRect().left) / ref.current.offsetWidth

        if (value > 1) {
            value = 1
        } else if (value < 0) {
            value = 0
        }

        value = Math.round(value * 100)

        if (value !== gradient[index].percent) {
            let newGradient = [...gradient]
            newGradient[index].percent = value
            setGradient(newGradient)
        }
    }

    const addMarker = (e) => {
        let value = (e.clientX - ref.current.getBoundingClientRect().left) / ref.current.offsetWidth

        if (value > 1) {
            value = 1
        } else if (value < 0) {
            value = 0
        }

        value = Math.round(value * 100)

        console.log(value)

        let newGradient = [...gradient, {percent: value, color: 'ff0000'}]
        setGradient(newGradient)
    }

    const deleteMarker = (i, e) => {
        let newGradient = [...gradient]
        newGradient.splice(i, 1)
        setGradient(newGradient)
    }

    let cssGradient = `linear-gradient(to right, ${gradient.sort((a, b) => {
        return a.percent - b.percent
    }).map(value => `#${value.color} ${value.percent}%`).join(", ")})`


    return (
        <div ref={ref} className={classes.SliderArea}>
            {
                gradient.map((value, index) => (
                    <div className={classes.Slider} style={{left: `${value.percent}%`}} key={index}>
                        <input
                            className={classes.SliderButton}
                            type="color"
                            id="head"
                            name="head"
                            value={`#${value.color}`}
                            onMouseDown={e => startMove(index)}
                            onDoubleClick={(event) => {
                                deleteMarker(index, event)
                            }}
                            onChange={event => {
                                updateColor(index, event.target.value.substr(1))
                            }
                            }/>
                    </div>
                ))
            }
            <div className={classes.Fill} style={{background: cssGradient}} onDoubleClick={addMarker}/>
        </div>
    );
}