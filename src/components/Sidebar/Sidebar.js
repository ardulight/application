import classes from "./Sidebar.module.css"
import {SelectList} from "../common/SelectList/SelectList";

export function Sidebar({items, onClick}) {
    return (
        <div className={classes.sidebar}>
            <div className={classes.title}>
                Ardulight
            </div>
            <div>
                <SelectList items={items} selectedIndex={0}></SelectList>
            </div>
            <div>
                Settings
            </div>
        </div>
    );
}