import classes from "./Select.module.css";
import {useState} from "react";
import {ListPopup} from "./ListPopup/ListPopup";

export function Select({items, selectedIndex, onSelect}) {
    const [popupIsVisible, setPopupVisible] = useState(false)
    const [_selectedIndex, setSelectedIndex] = useState(selectedIndex)


    const updateSelected = (index, value) => {
        setPopupVisible(false)
        setSelectedIndex(index)
        if (onSelect)
            onSelect(index, value)
    }

    return (
        <div className={classes.container}>
            <ListPopup
                visible={popupIsVisible}
                items={items}
                selectedIndex={_selectedIndex}
                onSelect={updateSelected}
                onCloseAreaClick={() => setPopupVisible(false)}
            />
            <div className={classes.select} onMouseUp={() => setPopupVisible(true)}>
                <div>
                    {items[_selectedIndex].title}
                </div>
                <svg viewBox="0 0 55 55" width="17" height="17">
                    <g className={classes.DropdownIcon}>
                        <path d="M31.836,43.006c0.282-0.281,0.518-0.59,0.725-0.912L54.17,20.485c2.107-2.109,2.109-5.528,0-7.638
                        c-2.109-2.107-5.527-2.109-7.638,0l-18.608,18.61L9.217,12.753c-2.109-2.108-5.527-2.109-7.637,0
                        C0.527,13.809-0.002,15.19,0,16.571c-0.002,1.382,0.527,2.764,1.582,3.816l21.703,21.706c0.207,0.323,0.445,0.631,0.729,0.913
                        c1.078,1.078,2.496,1.597,3.91,1.572C29.336,44.604,30.758,44.084,31.836,43.006z"/>
                    </g>
                </svg>
            </div>
        </div>
    );
}