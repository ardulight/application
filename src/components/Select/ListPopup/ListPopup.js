import classes from "./ListPopup.module.css";
import {useRef} from "react";
import {CSSTransition} from "react-transition-group";

export function ListPopup({visible, items, selectedIndex, onSelect, onCloseAreaClick}) {
    const _selectedIndex = useRef(selectedIndex)
    const listRef = useRef(null)

    let topHeight = 100


    if (_selectedIndex.current < 3) {
        topHeight = _selectedIndex.current * 50
    } else if (items.length - _selectedIndex.current < 4) {
        topHeight = 250 - (items.length - _selectedIndex.current) * 50
    }

    const popupStyle = {
        top: `${-topHeight}px`,
    }


    return (
        <CSSTransition
            in={visible}
            timeout={200}
            mountOnEnter
            unmountOnExit
            onEnter={() => {
                let scrollHeight = 0
                if (_selectedIndex.current < 3) {
                    scrollHeight = 0;
                } else if (items.length - _selectedIndex.current < 4) {
                    scrollHeight = listRef.current.scrollHeight
                } else {
                    scrollHeight = 50 * (_selectedIndex.current - 2)
                }
                listRef.current.scrollTop = scrollHeight
            }
            }
            classNames={{
                enterActive: classes.WrapEntering,
                exitActive: classes.WrapExiting,
                enterDone: classes.WrapShow,
            }}
            onExit={() => {
                _selectedIndex.current = selectedIndex
            }}
        >
            <div className={classes.Wrap}>
                <div className={classes.container} onClick={onCloseAreaClick}/>
                <div className={classes.popup} style={popupStyle}>
                    <div ref={listRef} className={classes.popupContainer} style={popupStyle}>
                        {
                            items.map((value, index) => (
                                <button onClick={() => onSelect(index, value)} key={index}>
                                    {value.title}
                                </button>
                            ))
                        }
                    </div>
                </div>
            </div>
        </CSSTransition>
    );
}