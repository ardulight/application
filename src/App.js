import './App.module.css';
import {Sidebar} from "./components/Sidebar/Sidebar";
import classes from "./App.module.css"
import {WidgetList} from "./components/WidgetList/WidgetList";


function App() {
  let items = [
      {label: "Noise palette", value: {}},
      {label: "Custom noise palette", value: {}},
      {label: "Liner palette", value: {}},
      {label: "Custom liner palette", value: {}},
      {label: "Solid color", value: {}},
  ]

  return (
    <div className={classes.app}>
        <div className={classes.sidebar}>
            <Sidebar items={items}/>
        </div>
        <div className={classes.content}>
            <WidgetList title={'Solid color'}/>
        </div>
    </div>
  );
}

export default App;
